document.addEventListener("contextmenu", function (e) {
  e.preventDefault();

  var tempDiv = document.createElement("div");
  tempDiv.style.position = "absolute";
  tempDiv.style.left = e.clientX + "px";
  tempDiv.style.top = e.clientY + "px";
  tempDiv.style.fontSize = "20px";
  tempDiv.style.transform = "translate(-50%, -100%)"; // Ajout de cette ligne pour déplacer l'élément au-dessus du curseur
  tempDiv.innerHTML = "🚫";
  tempDiv.style.zIndex = "9999";

  document.body.appendChild(tempDiv);

  setTimeout(function () {
    tempDiv.remove();
  }, 1000);
}, false);

document.addEventListener("keydown", function (e) {
  e.preventDefault();
});
document.addEventListener('DOMContentLoaded', function () {
  document.addEventListener('selectstart', function (e) {
    e.preventDefault();
  });
});
function changeLanguage(selectElement) {
  var selectedValue = selectElement.value;
  if (selectedValue === 'en') {
    window.location.href = '../English/index.html'; // Remplacez 'english.html' par l'URL de votre fichier en anglais
  } else if (selectedValue === 'fr') {
    window.location.href = 'index.html'; // Remplacez 'french.html' par l'URL de votre fichier en français
  }
}
